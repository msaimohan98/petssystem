package com.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.dao.UserDao;
import com.dao.UserDaoImpl;
import com.model.Cart;
import com.model.PetList;
import com.model.User;

@Controller
public class UserController {
	UserDao dao = new UserDaoImpl();

	@RequestMapping("regiter")
	public ModelAndView UserLogin() {
		return new ModelAndView("register", "r", new User());
	}

	@RequestMapping("UserRegistration")
	public ModelAndView UserLogin(@ModelAttribute("r") User user) {
		dao.userLogin(user);
		return new ModelAndView("display", "r", user);

	}

	@RequestMapping("login")
	public ModelAndView login() {
		return new ModelAndView("login", "u", new User());

	}

	@RequestMapping("userlogin")
	public ModelAndView loginUser(@RequestParam("uname") String uname, @RequestParam("password") String password,
			@ModelAttribute("u") User user) {
		UserDaoImpl dao = new UserDaoImpl();
		boolean isValidUser = dao.checklogin(uname, password);
		if (isValidUser) {
			return new ModelAndView("success", "loginsuccess", "Login Success!");
		} else {
			return new ModelAndView("userlogin", "loginsuccess", "Login Fail!");
		}
	}

	@RequestMapping("view")
	public ModelAndView view() {
		List<PetList> list = dao.viewpets();

		return new ModelAndView("view", "v", list);

	}

	@RequestMapping("add")
	public ModelAndView insertpets() {
		return new ModelAndView("add", "a", new PetList());

	}

	@RequestMapping("Addpets")
	public ModelAndView addpets(@ModelAttribute("a") PetList list2) {
		dao.addpets(list2);
		return new ModelAndView("display", "a", list2);
	}

	@RequestMapping("viewbuypets")
	public ModelAndView viewBuyPet(@RequestParam("pname") String pname, @RequestParam("pgender") String pgender,
			@RequestParam("paddress") String address, PetList list3) {
		List<PetList> list = dao.getByPetName(list3);
		list.add(list3);

		return new ModelAndView("details", "view", list3);

	}

	@RequestMapping("pets")
	public ModelAndView mypets(@ModelAttribute("userpets") Cart mypets) {

		dao.insertIntoMyPets(mypets);
		return new ModelAndView("getmypetspage", "userpets", mypets);
	}

	@RequestMapping("mypet")
	public ModelAndView addpets(@ModelAttribute("userpets") Cart mypets) {

		return new ModelAndView("buyer", "userpets", new Cart());

	}

	@RequestMapping("cart")
	public ModelAndView mycart() {
		List<Cart> list = dao.viewmypets();
		return new ModelAndView("display", "pets", list);
	}

	@RequestMapping("logout")
	public ModelAndView logout() {
		return new ModelAndView("logout");
	}

}
