package com.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="mypets")
public class Cart {
	private String uname;
	@Id
	private String pname;
	private String pgender;
	private String paddress;
	
	public Cart() {
		
	}
	
	public Cart(String uname, String pname, String pgender, String paddress) {
		super();
		this.uname = uname;
		this.pname = pname;
		this.pgender = pgender;
		this.paddress = paddress;
	}

	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	public String getPgender() {
		return pgender;
	}
	public void setPgender(String pgender) {
		this.pgender = pgender;
	}
	public String getPaddress() {
		return paddress;
	}
	public void setPaddress(String paddress) {
		this.paddress = paddress;
	}
	
	

}
