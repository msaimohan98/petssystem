package com.dao;

import java.util.List;

import com.model.Cart;
import com.model.PetList;
import com.model.User;

public interface UserDao {
	public void  userLogin(User user);
	public  boolean checklogin(String uname,String password);
	public List<PetList> viewpets();
	public void addpets(PetList list2);

	public List<PetList> getByPetName(PetList list3);

	public void insertIntoMyPets(Cart mypets);

	public List<Cart> viewmypets();

}
