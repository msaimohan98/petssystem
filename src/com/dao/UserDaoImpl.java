package com.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import com.model.Cart;
import com.model.PetList;
import com.model.User;

public class UserDaoImpl implements UserDao {

	@Override
	public void userLogin(User user) {
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		session.save(user);
		tx.commit();

	}

	

	@SuppressWarnings("rawtypes")
	@Override
	public boolean checklogin(String uname, String password) {
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		boolean isValidUser = false;
		Query query = session.createQuery("from user where uname=?1 and password=?2");
		query.setParameter(1, uname);
		query.setParameter(2, password);
		List list = query.list();
		if (list != null && list.size() > 0) {
			isValidUser = true;
		}
		tx.commit();
		return isValidUser;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PetList> viewpets() {
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().openSession();
		session.beginTransaction();
		List<PetList> list = new ArrayList<PetList>();
		@SuppressWarnings("rawtypes")
		Query q = session.createQuery("from PetList");
		List<PetList> li = q.list();
		for (PetList petDetails : li) {
			list.add(petDetails);
		}

		return list;
	}

	@Override
	public void addpets(PetList list2) {
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		session.save(list2);
		tx.commit();
	}

	@Override
	public List<PetList> getByPetName(PetList list3) {
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().openSession();
		session.beginTransaction();
		List<PetList> list = new ArrayList<PetList>();
		PetList pet = new PetList();
		String q = ("from Petlist where petname=?1");
		if (!q.isEmpty()) {
			@SuppressWarnings("rawtypes")
			Query query = session.createQuery(q);
			query.setParameter(1, pet.getPname());
			for (PetList  list4 : list) {
				pet.getPname();
				pet.setPgender(list4.getPgender());
				pet.setPaddress(list4.getPaddress());
				pet.setPcost(list4.getPcost());
				
			}

		}
		return list;
	}

	@Override
	public void insertIntoMyPets(Cart mypets) {
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		session.save(mypets);
		tx.commit();
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Cart> viewmypets() {
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().openSession();
		session.beginTransaction();
		List<Cart> list = new ArrayList<Cart>();
		@SuppressWarnings("rawtypes")
		Query q = session.createQuery("from Cart ");
		List<Cart> li = q.list();
		for (Cart mycart : li) {
			list.add(mycart);
		}

		return list;
	}

	
	}

	


