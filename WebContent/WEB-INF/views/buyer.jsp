<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<style>
body {
	background-color: lightblue;
}
</style>
<body>
	<c:form id="insert" action="pets" method="post">
		<table>
			<tr>
				<th>uname</th>
				<td><input name="uname" type="text"></td>
			</tr>

			<tr>
				<th>pet name</th>
				<td><input name="petname" type="text" value="${p.getPname()}"></td>
			</tr>
			<tr>
				<th>pet gender</th>
				<td><input name="petgender" type="text"
					value="${p.getPgender()}"></td>
			</tr>
			<tr>
				<th>pet address</th>
				<td><input name="petaddress" type="text"
					value="${p.getPaddress()}"></td>
			</tr>
			<tr>
			<tr>
				<th colspan="2"><input type="submit" value="Submit"></th>
			</tr>
		</table>


	</c:form>

</body>
</html>