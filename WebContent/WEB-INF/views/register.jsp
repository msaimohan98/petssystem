<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<style>
body {
	background-color: lightblue;
}

h1 {
	color: black;
	align: left;
}

h2 {
	color: black;
	align: left;
	font-size: 30px;
}
</style>
<body>
	<h1>Welcome to RegistrationForm</h1>
	<hr>
	<h2>Create account</h2>
	<c:form action="UserRegistration" method="post" modelAttribute="r">
		<table>
			<tr>

				<td><c:input path="uname" autocomplete="off"/>
				</td>
			</tr>
			<tr>

				<td><c:password path="password" autocomplete="off"  />
				</td>
			</tr>
<tr>
				<td><c:button>Register</c:button></td>
			</tr>
			
		</table>
		<a href="index.jsp">Homepage</a>
	</c:form>
<body>

</body>
</html>