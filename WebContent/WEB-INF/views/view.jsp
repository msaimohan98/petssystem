<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<style>
body {
	background-color: lightblue;
}

h1 {
	color: black;
	align: center;
	font-size: 40px;
}

h1 {
	color: black;
	align:;
	font-size: 40px;
}
</style>
<body>
	<h1>Welcome to Pets Gallery</h1>
	<hr>
	<h2>Views All Pets</h2>
	<table>
		<tr>
			<th>Pet Name</th>
			<th>Pet gender</th>
			<th>Pet address</th>
			<th>Pet cost</th>
			<th>Pet Status</th>
		</tr>
		<c:forEach items="${view}" var="v">
			<tr>
				<td>${v.getPname()}</td>
				<td>${v.getPgender()}</td>
				<td>${v.getPaddress)}</td>
				<td>${v.getPcost()}</td>
				<td>${v.getstatus()}</td>
				<td><a
					href="viewbuypets?pet=${v.getPet()}&petname=${v.getPetname()}&petgender=${v.getPetgender()}
				
				&petaddress=${v.getPaddress()}&cost=${v.getPCost()}&status=${v.getStatus()}">Buy</a></td>
			</tr>
			<td>
		</c:forEach>
	</table>
	<hr /><br/><br/>
	<a href="index.jsp">Home</a>
	<a href="add">Add Pets Details</a>
	<a href="logout">Logout</a>
</body>
</html>