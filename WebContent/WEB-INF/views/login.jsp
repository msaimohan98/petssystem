<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib uri="http://www.springframework.org/tags/form" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<style>
body {
	background-color: lightblue;
}

h1 {
	color: black;
	align: left;
}
</style>
<body>
	<h1>LoginForm</h1>
	<hr>
	<c:form action="Userlogin" method="post" modelAttribute="u">
		<table>
			<tr>

				<td><c:input path="uname"  autocomplete="off"/></td>
			</tr>
			<tr>

				<td><c:password path="password" autocomplete="off" /></td>
			</tr>
<tr>
				<td><c:button>Login</c:button></td>
			</tr>

		</table>
		<a href="index.jsp">Homepage</a>
	</c:form>
	/body>
</html>